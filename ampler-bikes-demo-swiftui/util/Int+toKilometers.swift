import Foundation

extension Int {
  var toKilometers: Double {
    (self.toDouble / 1000).roundTo(decimalPlaces: 2)
  }
}

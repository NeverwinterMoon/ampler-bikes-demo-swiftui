import Foundation

extension Double {
  var roundedToOneDecimalPlace: Double {
    self.roundTo(decimalPlaces: 1)
  }
}

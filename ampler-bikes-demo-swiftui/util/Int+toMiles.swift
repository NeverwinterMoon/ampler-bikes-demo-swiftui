import Foundation

extension Int {
  var toMiles: Double {
    (self.toDouble / 1609.344).roundTo(decimalPlaces: 2)
  }
}

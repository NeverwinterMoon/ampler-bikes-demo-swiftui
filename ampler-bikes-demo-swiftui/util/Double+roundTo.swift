import Foundation

extension Double {
  func roundTo(decimalPlaces: Int) -> Double {
    let divisor = pow(10.0, Double(decimalPlaces))

    return (self * divisor).rounded() / divisor
  }
}

import Foundation

extension Int {
  var toDouble: Double {
    Double(self)
  }
}

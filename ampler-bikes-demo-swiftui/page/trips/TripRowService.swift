import Foundation

class TripRowService {
  private let userDefaultsService: UserDefaultsServiceType

  init(userDefaultsService: UserDefaultsServiceType) {
    self.userDefaultsService = userDefaultsService
  }

  convenience init() {
    self.init(userDefaultsService: UserDefaultsService.shared)
  }

  var unitOfLength: LengthUnit {
    get { userDefaultsService.unitOfLength }
    set { userDefaultsService.unitOfLength = newValue }
  }
}

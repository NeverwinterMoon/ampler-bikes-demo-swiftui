import SwiftUI

struct TripRow: View {
  @EnvironmentObject private var viewModel: TripRowViewModel

  var body: some View {
    Text(verbatim: String(viewModel.adjustedDistance))
  }
}

struct TripRow_Previews: PreviewProvider {
  static var previews: some View {
    let trip = Trip()
    trip.distanceInMeters = 100
    let viewModel = TripRowViewModel(trip: trip, service: TripRowService())

    return TripRow().environmentObject(viewModel)
  }
}

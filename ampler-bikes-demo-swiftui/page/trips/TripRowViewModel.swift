import Foundation

class TripRowViewModel: ObservableObject, Identifiable {
  private let service: TripRowService
  private let trip: Trip

  var id: String {
    String(trip.distanceInMeters)
  }

  init(trip: Trip, service: TripRowService) {
    self.service = service
    self.trip = trip
  }

  var adjustedDistance: Double {
    switch service.unitOfLength {
      case .km:
        return Int(trip.distanceInMeters).toKilometers

      case .m:
        return Int(trip.distanceInMeters).toMiles
    }
  }
}

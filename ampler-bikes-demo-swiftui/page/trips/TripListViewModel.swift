import CoreData
import Foundation

class TripListViewModel: ObservableObject, Identifiable {
  private var managedObjectContext: NSManagedObjectContext

  @Published var dataSource = [TripRowViewModel]()

  init(managedObjectContext: NSManagedObjectContext) {
    self.managedObjectContext = managedObjectContext

    fetch()
  }

  func fetch() {
    if let trips = try? managedObjectContext.fetch(Trip.fetchTrips()) {
      dataSource = trips.map { TripRowViewModel(trip: $0, service: TripRowService()) }
    } else {
      dataSource = []
    }
  }
}

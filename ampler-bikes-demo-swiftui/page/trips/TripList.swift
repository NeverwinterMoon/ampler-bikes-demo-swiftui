import CoreData
import SwiftUI

struct TripList: View {
  @Environment(\.managedObjectContext) var managedObjectContext
  @EnvironmentObject private var viewModel: TripListViewModel

  var body: some View {
    List(viewModel.dataSource) { viewModel in
      TripRow().environmentObject(viewModel)
    }
      .navigationBarTitle(Text("Trips"))
  }
}

struct TripList_Previews: PreviewProvider {
  static var previews: some View {
    TripList()
  }
}

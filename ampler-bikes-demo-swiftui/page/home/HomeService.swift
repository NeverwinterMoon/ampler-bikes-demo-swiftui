import Combine
import CoreData
import CoreLocation
import SwiftUI
import UIKit

class HomeViewService {
  private let locationService: LocationService
  private let userDefaultsService: UserDefaultsServiceType
  private lazy var context: NSManagedObjectContext = {
    let appDelegate = UIApplication.shared.delegate as? AppDelegate

    return appDelegate!.persistentContainer.viewContext
  }()

  init(locationService: LocationService, userDefaultsService: UserDefaultsServiceType) {
    self.locationService = locationService
    self.userDefaultsService = userDefaultsService
  }

  convenience init() {
    self.init(
      locationService: LocationService.shared,
      userDefaultsService: UserDefaultsService.shared
    )
  }

  func requestLocationPermission() -> AnyPublisher<Bool, Error> {
    locationService.requestPermission()

    return locationService.locationPermissionPublisher
  }

  func startUpdatingLocation() -> AnyPublisher<CLLocation, Error> {
    locationService.startUpdatingLocation()

    return locationService.locationUpdating
  }

  func stopUpdatingLocation() {
    locationService.stopUpdatingLocation()
  }

  var unitOfLength: LengthUnit {
    get { userDefaultsService.unitOfLength }
    set { userDefaultsService.unitOfLength = newValue }
  }

  func addTrip(distanceInMeters distance: Int) {
    let trip = Trip(context: context)
    trip.distanceInMeters = Int64(distance)
    context.save(with: .addTrip)
  }
}

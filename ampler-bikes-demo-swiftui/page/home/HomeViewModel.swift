import Combine
import CoreData
import CoreLocation
import Foundation

class HomeViewModel: ObservableObject, Identifiable {
  @Published var isLocationPermissionGranted: PermissionStatus = .pending
  @Published var distance: Int = 0
  @Published var isTracking: Bool = false
  @Published var defaultUnitOfLength: Bool {
    willSet {
      if newValue == true {
        service.unitOfLength = .km
      } else {
        service.unitOfLength = .m
      }
    }
  }

  var locations = [CLLocation]()

  // lifecycle
  private var disposables = Set<AnyCancellable>()
  private let backgroundQueue: DispatchQueue = DispatchQueue(label: "HomeViewModel background queue")

  private let service: HomeViewService

  init(service: HomeViewService) {
    self.service = service

    defaultUnitOfLength = service.unitOfLength == .km
  }

  func requestLocationPermission() {
    service
      .requestLocationPermission()
      .sink(receiveCompletion: { _ in }) { [weak self] isGranted in
        if isGranted {
          self?.isLocationPermissionGranted = .granted
        } else {
          self?.isLocationPermissionGranted = .notGranted
        }
      }
      .store(in: &disposables)
  }

  func startTracking() {
    isTracking = true

    service
      .startUpdatingLocation()
      .receive(on: RunLoop.main)
      .delay(for: 1, scheduler: RunLoop.main)
      .sink(receiveCompletion: { _ in }) { location in
        if let lastLocation = self.locations.last {
          self.distance += Int(location.distance(from: lastLocation))
        }

        self.locations.append(location)
      }
      .store(in: &disposables)
  }

  func stopTracking() {
    service.stopUpdatingLocation()
    isTracking = false

    service.addTrip(
      distanceInMeters: distance
    )

    locations = []
    distance = 0
  }

  var adjustedDistance: Double {
    switch service.unitOfLength {
      case .km:
        return distance.toKilometers

      case .m:
        return distance.toMiles
    }
  }
}

import SwiftUI

struct CurrentlyTrackingView: View {
  @EnvironmentObject private var viewModel: HomeViewModel

  var body: some View {
    VStack {
      button(for: viewModel)
      distance(for: viewModel)
    }
  }
}

private extension CurrentlyTrackingView {
  func button(for viewModel: HomeViewModel) -> some View {
    Button(action: { viewModel.stopTracking() }) {
      Text("Stop tracking")
    }
  }

  func distance(for viewModel: HomeViewModel) -> some View {
    Text(verbatim: String(viewModel.adjustedDistance))
  }
}

struct CurrentlyTrackingView_Previews: PreviewProvider {
  static var previews: some View {
    let viewModel = HomeViewModel(service: HomeViewService())

    return CurrentlyTrackingView().environmentObject(viewModel)
  }
}

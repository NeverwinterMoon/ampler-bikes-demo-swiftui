import SwiftUI

struct NotTrackingView: View {
  @EnvironmentObject private var viewModel: HomeViewModel

  var body: some View {
    let button = Button(action: { self.viewModel.startTracking() }) {
      Text("Start tracking")
    }

    return VStack {
      button
      Text(verbatim: String(self.viewModel.distance))
    }
  }
}

struct NotTrackingView_Previews: PreviewProvider {
  static var previews: some View {
    let viewModel = HomeViewModel(service: HomeViewService())

    return NotTrackingView().environmentObject(viewModel)
  }
}

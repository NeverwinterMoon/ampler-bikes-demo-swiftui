import Combine
import SwiftUI

struct HomeView: View {
  @Environment(\.managedObjectContext) var managedObjectContext
  @EnvironmentObject private var viewModel: HomeViewModel

  var body: some View {
    NavigationView(content: mainContent)
      .onAppear {
        self.viewModel.requestLocationPermission()
      }
  }

  func mainContent() -> some View {
    let output: AnyView
    switch viewModel.isLocationPermissionGranted {
      case .granted:
        if self.viewModel.isTracking {
          output = AnyView(tracking(for: viewModel))
        } else {
          output = AnyView(notTracking(for: viewModel))
        }

      case .notGranted:
        output = AnyView(noLocationPermissions)

      case .pending:
        output = AnyView(checkingPermissions)
    }

    return output.navigationBarItems(
      leading: HStack {
        Text("unit: ")

        Button(
          action: { self.viewModel.defaultUnitOfLength.toggle() },
          label: { Text(self.viewModel.defaultUnitOfLength ? LengthUnit.km.rawValue : LengthUnit.m.rawValue) }
        )
      },
      trailing: NavigationLink(destination: TripList().environmentObject(TripListViewModel(managedObjectContext: managedObjectContext))) {
        Text("previous trips")
      }
    )
  }
}

private extension HomeView {
  func tracking(for viewModel: HomeViewModel) -> some View {
    CurrentlyTrackingView().environmentObject(viewModel)
  }

  func notTracking(for viewModel: HomeViewModel) -> some View {
    NotTrackingView().environmentObject(viewModel)
  }

  var noLocationPermissions: some View {
    VStack(alignment: .center, spacing: 8.0) {
      Text("I don't have the permission to check your location, I am useless without it!")
        .multilineTextAlignment(.center)

      Text(
        "(this app does not appear in settings, so to reset location permissions, go to Settings > Reset > Reset Location & Privacy)")
        .multilineTextAlignment(.center)
        .font(.system(size: 10))
        .foregroundColor(.gray)
        .multilineTextAlignment(.center)

      Button(action: {
        guard let settingsURL = URL(string: UIApplication.openSettingsURLString) else { return }

        UIApplication.shared.open(settingsURL)
      }) {
        Text("go to settings".uppercased())
          .frame(maxWidth: .infinity, minHeight: 40)
          .border(Color.gray, width: 1)
      }

      Button(
        action: { self.viewModel.requestLocationPermission() },
        label: {
          Text("check permissions again".uppercased())
            .frame(maxWidth: .infinity, minHeight: 40)
            .border(Color.gray, width: 1)
        }
      )
    }.padding(.horizontal, 24)
  }

  var checkingPermissions: some View {
    Text("Checking location permissions")
  }
}

struct HomeView_Previews: PreviewProvider {
  static var previews: some View {
    return HomeView()
      .environmentObject(HomeViewModel(service: HomeViewService()))
  }
}

enum PermissionStatus {
  case pending
  case granted
  case notGranted
}

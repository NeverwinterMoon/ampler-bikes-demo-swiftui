import Combine
import CoreLocation
import Foundation

class LocationService: NSObject {
  static let shared = LocationService()

  let locationPermissionPublisher: AnyPublisher<Bool, Error>
  private let _locationPermissionPublisher = CurrentValueSubject<Bool, Error>(false)

  let locationUpdating: AnyPublisher<CLLocation, Error>
  private let _locationUpdating = PassthroughSubject<CLLocation, Error>()

  private let locationManager: CLLocationManager
  private var disposables = Set<AnyCancellable>()

  init(locationManager: CLLocationManager) {
    self.locationManager = locationManager

    locationPermissionPublisher = _locationPermissionPublisher.eraseToAnyPublisher()
    locationUpdating = _locationUpdating.eraseToAnyPublisher()

    super.init()
    locationManager.delegate = self
  }

  override convenience init() {
    let locationManager: CLLocationManager = {
      let locationManager = CLLocationManager()
      locationManager.distanceFilter = 1
      locationManager.allowsBackgroundLocationUpdates = true
      locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation

      return locationManager
    }()

    self.init(
      locationManager: locationManager
    )
  }

  func requestPermission() {
    locationManager.requestAlwaysAuthorization()
  }

  func startUpdatingLocation() {
    locationManager.startUpdatingLocation()
  }

  func stopUpdatingLocation() {
    locationManager.stopUpdatingLocation()
  }
}

extension LocationService: CLLocationManagerDelegate {
  // This is called immediately on CLLocationManager init, that's why we are using CurrentValueSubject and not PassthroughSubject
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    switch status {
      case .authorizedAlways:
        _locationPermissionPublisher.send(true)

      default:
        _locationPermissionPublisher.send(false)
    }
  }

  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if let location = locations.last {
      _locationUpdating.send(location)
    }
  }
}

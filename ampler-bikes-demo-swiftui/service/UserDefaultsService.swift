import Foundation

class UserDefaultsService: UserDefaultsServiceType {
  static let shared = UserDefaultsService()

  private let userDefaults: UserDefaults

  init(userDefaults: UserDefaults) {
    self.userDefaults = userDefaults
  }

  private convenience init() {
    self.init(
        userDefaults: UserDefaults.standard
    )
  }

  var unitOfLength: LengthUnit {
    get {
      if let unitString = userDefaults.string(forKey: UserDefaultsKey.unitOfLength),
         let unit = LengthUnit(rawValue: unitString) {
        return unit
      } else {
        return LengthUnit.km
      }
    }
    set { userDefaults.setValue(newValue.rawValue, forKey: UserDefaultsKey.unitOfLength) }
  }
}

protocol UserDefaultsServiceType {
  var unitOfLength: LengthUnit { get nonmutating set }
}

struct UserDefaultsKey {
  static let unitOfLength = "unit of length"
}

import Foundation

enum LengthUnit: String {
  case m
  case km
}

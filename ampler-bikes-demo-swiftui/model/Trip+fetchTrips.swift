import CoreData
import Foundation

extension Trip {
  static func fetchTrips() -> NSFetchRequest<Trip> {
    let request: NSFetchRequest<Trip> = Trip.fetchRequest()
    request.sortDescriptors = [NSSortDescriptor(key: "distanceInMeters", ascending: true)]

    return request
  }
}

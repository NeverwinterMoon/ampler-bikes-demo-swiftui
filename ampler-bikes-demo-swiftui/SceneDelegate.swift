import SwiftUI
import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

  var window: UIWindow?

  func scene(
    _ scene: UIScene,
    willConnectTo session: UISceneSession,
    options connectionOptions: UIScene.ConnectionOptions
  ) {

    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    if let windowScene = scene as? UIWindowScene {
      let window = UIWindow(windowScene: windowScene)
      let viewModel = HomeViewModel(service: HomeViewService())
      let homeView = HomeView()
        .environmentObject(viewModel)
        .environment(\.managedObjectContext, managedObjectContext)
      window.rootViewController = UIHostingController(rootView: homeView)
      self.window = window
      window.makeKeyAndVisible()
    }
  }
}


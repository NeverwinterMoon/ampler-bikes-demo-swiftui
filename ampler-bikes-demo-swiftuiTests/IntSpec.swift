import XCTest

@testable import ampler_bikes_demo_swiftui

class ampler_bikes_demo_swiftuiTests: XCTestCase {
  func test_Int_toKilometers() {
    let meters = 50

    XCTAssertEqual(meters.toKilometers, 0.05, "Wrong conversion from meters to kilometers")
  }

  func test_Int_toMiles() {
    let meters = 50

    XCTAssertEqual(meters.toMiles, 0.03, "Wrong conversion from meters to miles")
  }
}
